import isValid from '../../utility/isValid';

describe('IsValidTest', () => {
  it('isValid_noValidationRules_true', () => {
    const validationRules = {};
    const text = 'Input';
    const data = isValid(text, validationRules);
    expect(data).toEqual(true);
  })

  it('isValid_requireRuleAndValidText_true', () => {
    const validationRules = {
      required: true
    }
    const text = 'Input';
    const data = isValid(text, validationRules);
    expect(data).toEqual(true);
  })

  it('isValid_requiredRuleAndEmptyText_false', () => {
    const validationRules = {
      required: true
    }
    const text = ''
    const data = isValid(text, validationRules);
    expect(data).toEqual(false);
  })

  it('isValid_minLengthRulesAndValidTextLength_true', () => {
    const validationRules = {
      minLength: 3
    }
    const text = 'Input'
    const data = isValid(text, validationRules);
    expect(data).toEqual(true);
  })

  it('isValid_minLengthRulesAndInvalidTextLength_false', () => {
    const validationRules = {
      minLength: 3
    }
    const text = 'In'
    const data = isValid(text, validationRules);
    expect(data).toEqual(false);
  })

  it('isValid_maxLengthRulesAndValidTextLength_true', () => {
    const validationRules = {
      maxLength: 5
    }
    const text = 'Inp'
    const data = isValid(text, validationRules);
    expect(data).toEqual(true);
  })

  it('isValid_maxLengthRulesAndInvalidTextLength_false', () => {
    const validationRules = {
      maxLength: 3
    }
    const text = 'Input'
    const data = isValid(text, validationRules);
    expect(data).toEqual(false);
  })

  it('isValid_isEmailRuleAndValidEmailForm_true', () => {
    const validationRules = {
      isEmail: true
    }
    const text = 'user@gmail.com';
    const data = isValid(text, validationRules);
    expect(data).toEqual(true);
  })

  it('isValid_isEmailRuleAndInvalidEmailForm_false', () => {
    const validationRules = {
      isEmail: true
    }
    const text = 'user';
    const data = isValid(text, validationRules);
    expect(data).toEqual(false)
  })
})
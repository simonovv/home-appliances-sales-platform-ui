import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import React from 'react';

import Input from '../../../Components/UI/Input/Input';

Enzyme.configure({adapter: new Adapter()});

describe('InputConmponentTest', () => {
  it('render_inputType_inputElement', () => {
    const wrapper = Enzyme.shallow(<Input />);
    wrapper.setProps({elementType: 'input'});
    const expectedElement = <input />;
    expect(wrapper.find(expectedElement));
  })

  it('render_noInputType_inputElement', () => {
    const wrapper = Enzyme.shallow(<Input />);
    const expectedElement = <input />;
    expect(wrapper.find(expectedElement));
  })

  it('render_InputTypeTextarea_textareaElement', () => {
    const wrapper = Enzyme.shallow(<Input />);
    wrapper.setProps({elementType: 'textarea'});
    const expectedElement = <textarea />;
    expect(wrapper.find(expectedElement));
  })
})



import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import configureStore from 'redux-mock-store';
import React from 'react';
import { MemoryRouter } from 'react-router-dom';
import Item from '../../../Components/Item/Item';

import { Items } from '../../../Containers/Items/Items';

Enzyme.configure({adapter: new Adapter()});

const mockStore = configureStore();
const initialState = {

}

const store = mockStore(initialState);

describe('ItemsContainerTets', () => {
  it('searchItemsHandler_objectWithSearchedName_arrayWithFindItems', () => {
    const comp = (
      <Items store={store}/>
    )
    const match = {
      params: {
        id: 1
      }
    }
    const event = {
      target: {
        value: 'M'
      }
    }
    const wrapper = Enzyme.shallow(comp);
    const items = [
      {id: 1, name: 'MacBook'},
      {id: 2, name: 'Heroku'}
    ]
    wrapper.setProps({items: items});
    wrapper.instance().searchHandler(event)
    const data = wrapper.instance().state
    const expectedState = {
      items: [
        {id: 1, name: 'MacBook'}
      ],
      creatingNewItem: false
    }
    expect(data).toEqual(expectedState);
  })
})



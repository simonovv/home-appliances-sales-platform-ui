import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import configureStore from 'redux-mock-store';
import React from 'react';

import { Categories } from '../../../Containers/Categories/Categories';

Enzyme.configure({adapter: new Adapter()});

const mockStore = configureStore();
const initialState = {

}

const store = mockStore(initialState);

describe('CategoriesContainerTets', () => {
  it('searchCategoriesHandler_objectWithSearchedName_arrayWithFindCategories', () => {
    const comp = (
      <Categories store={store}/>
    )
    const event = {
      target: {
        value: 'M'
      }
    }
    const wrapper = Enzyme.shallow(comp);
    const categories = [
      {id: 1, name: 'MacBook'},
      {id: 2, name: 'Heroku'}
    ]
    wrapper.setProps({categories: categories});
    wrapper.instance().searchCategoryHandler(event)
    const data = wrapper.instance().state
    const expectedState = {
      categories: [
        {id: 1, name: 'MacBook'}
      ],
      creatingNewCategory: false
    }
    expect(data).toEqual(expectedState);
  })
})



import * as actionTypes from '../../../store/actions/actionTypes';
import reducer from '../../../store/reducers/items';

describe('ItemsReducerTest', () => {
  it('fetchAllItemsSuccess_actionWithItems_turnLoadingFalseSetItems', () => {
    const action = {
      type: actionTypes.FETCH_ALL_ITEMS_SUCCESS,
      items: []
    }

    const data = reducer({}, action);
    const expectedState = {
      loading: false,
      items: []
    }

    expect(data).toEqual(expectedState);
  })

  it('fetchAllItemsFailed_actionWithError_turnLoadingFalseSetError', () => {
    const action = {
      type: actionTypes.FETCH_ALL_ITEMS_FAILED,
      error: ''
    }

    const data = reducer({}, action);
    const expectedState = {
      loading: false,
      error: ''
    }

    expect(data).toEqual(expectedState);
  })

  it('fetchSingleItemSuccess_actionWithItem_turnLoadingFalseSetItem', () => {
    const action = {
      type: actionTypes.FETCH_SINGLE_ITEM_SUCCESS,
      item: {}
    }

    const data = reducer({}, action);
    const expectedState = {
      loading: false,
      item: {}
    }

    expect(data).toEqual(expectedState);
  })

  it('fetchSingleItemFailed_actionWithError_turnLoadingFalseSetError', () => {
    const action = {
      type: actionTypes.FETCH_SINGLE_ITEM_FAILED,
      error: ''
    }

    const data = reducer({}, action);
    const expectedState = {
      loading: false,
      error: ''
    }

    expect(data).toEqual(expectedState);
  })

  it('createItemSuccess_actionWithCreatedItem_turnLoadingToFalsePushItemToItems', () => {
    const action = {
      type: actionTypes.CREATE_ITEM_SUCCESS,
      item: {}
    }

    const state = {
      items: []
    }

    const data = reducer(state, action);
    const expectedState = {
      items: [{}],
      loading: false
    }

    expect(data).toEqual(expectedState);
  })

  it('createItemFailed_actionWithError_turnLoadingFalseSetItem', () => {
    const action = {
      type: actionTypes.CREATE_ITEM_FAILED,
      error: ''
    }

    const data = reducer({}, action);
    const expectedState = {
      error: '',
      loading: false
    }

    expect(data).toEqual(expectedState);
  })

  it('updateItemSuccess_action_turnLoadingFalse', () => {
    const action = {
      type: actionTypes.UPDATE_ITEM_SUCCESS
    }

    const data = reducer({}, action);
    const expectedState = {
      loading: false
    }

    expect(data).toEqual(expectedState);
  })

  it('updateItemFailed_actionWithError_turnLoadingFalseSetError', () => {
    const action = {
      type: actionTypes.UPDATE_ITEM_FAILED, 
      error: ''
    }

    const data = reducer({}, action);
    const expectedState = {
      loading: false, 
      error: ''
    }

    expect(data).toEqual(expectedState);
  })

  it('deleteItemFailed_actionWithError_turnLoadingFalseSetError', () => {
    const action = {
      type: actionTypes.DELETE_ITEM_FAILED,
      error: ''
    }

    const data = reducer({}, action);
    const expectedState = {
      loading: false,
      error: ''
    }

    expect(data).toEqual(expectedState);
  })

  it('deleteItemSucces_action_turnLoadingFalse', () => {
    const action = {
      type: actionTypes.UPDATE_ITEM_SUCCESS
    }

    const data = reducer({}, action);
    const expectedState = {
      loading: false
    }

    expect(data).toEqual(expectedState);
  })
})
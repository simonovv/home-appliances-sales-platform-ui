import * as actionTypes from '../../../store/actions/actionTypes';
import reducer from '../../../store/reducers/categories';

describe('CategoriesReducerTest', () => {
  it('fetchCategoriesStart_action_turnLoadingToTrue', () => {
    const action = {
      type: actionTypes.FETCH_CATEGORY_START
    }

    const data = reducer({}, action);
    const expectedState = {
      loading: true
    }

    expect(data).toEqual(expectedState);
  })

  it('fetchCategoriesSuccess_action_turnLoadingToFalseAnSetCategories', () => {
    const action = {
      type: actionTypes.FETCH_CATEGORY_SUCCESS,
      categories: []
    }

    const data = reducer({}, action);
    const expectedState = {
      loading: false,
      categories: []
    }

    expect(data).toEqual(expectedState);
  })

  it('fetchCategoriesFailed_action_turnLoadingToFalseSetError', () => {
    const action = {
      type: actionTypes.FETCH_CATEGORY_FAILED,
      error: ''
    }

    const data = reducer({}, action);
    const expectedState = {
      loading: false,
      error: ''
    }

    expect(data).toEqual(expectedState);
  })

  it('createCategorySuccess_action_turnLoadingToFalseAndAddCreateCategoryToCategories', () => {
    const action = {
      type: actionTypes.CREATE_CATEGORY_SUCCESS,
      category: {
        id: 1,
        name: 'Cat1',
        img: ''
      }
    }

    const state = {
      categories: []
    }

    const data = reducer(state, action);
    const expectedState = {
      categories: [
        {
          id: 1,
          name: 'Cat1',
          img: ''
        }
      ],
      loading: false
    }

    expect(data).toEqual(expectedState);
  })

  it('createCategoryFailed_action_turnLoadingToFalseSetError', () => {
    const action = {
      type: actionTypes.CREATE_CATEGORY_FAILED, 
      error: ''
    }

    const data = reducer({}, action);
    const expectedState = {
      loading: false,
      error: ''
    }

    expect(data).toEqual(expectedState);
  })

  it('updateCategorySuccess_action_turnLoadingFalseRefreshTrue', () => {
    const action = {
      type: actionTypes.UPDATE_CATEGORY_SUCCESS
    }

    const data = reducer({}, action);
    const expectedState = {
      loading: false,
      refresh: true
    }

    expect(data).toEqual(expectedState);
  })

  it('updateCategoryFailed_action_turnLoadingFalseSetError', () => {
    const action = {
      type: actionTypes.UPDATE_CATEGORY_FAILED,
      error: ''
    }

    const data = reducer({}, action);
    const expectedState = {
      loading: false,
      error: ''
    }

    expect(data).toEqual(expectedState);
  })

  it('deleteCategorySuccess_action_turnLoadingFalse', () => {
    const action = {
      type: actionTypes.DELETE_CATEGORY_SUCCESS
    }

    const data = reducer({}, action);
    const expectedState = {
      loading: false
    }

    expect(data).toEqual(expectedState);
  })

  it('deleteCategoryFailed_action_turnLoadingFalseSetError', () => {
    const action = {
      type: actionTypes.DELETE_CATEGORY_FAILED,
      error: ''
    }

    const data = reducer({}, action);
    const expectedState = {
      loading: false,
      error: ''
    }

    expect(data).toEqual(expectedState);
  })
})
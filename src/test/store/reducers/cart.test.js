import * as actionTypes from '../../../store/actions/actionTypes';
import reducer from '../../../store/reducers/cart';

describe('OrderReducerTest', () => {
  it('addItemToCart_actionWithItemThatAlreadyExists_turnAmountOfExistsItemPlusOne', () => {
    const action = {
      type: actionTypes.ADD_NEW_ITEM_TO_CART,
      item: {
        id: 1,
        name: '',
        price: 100,
        description: '',
        categoryId: 1
      }
    }

    const state = {
      cart: [
        {
          id: 1,
          name: '',
          price: 100,
          description: '',
          categoryId: 1,
          amount: 1
        }
      ]
    }

    const data = reducer(state, action);
    const expectedState = {
      cart: [
        {
          id: 1,
          name: '',
          price: 100,
          description: '',
          categoryId: 1,
          amount: 2
        }
      ]
    }

    expect(data).toEqual(expectedState)
  })

  it('addItemToCart_actionWithNotExistsItem_addItemToCartAndSetAmountTo1', () => {
    const action = {
      type: actionTypes.ADD_NEW_ITEM_TO_CART,
      item: {
        id: 1,
        name: '',
        price: 100,
        description: '',
        categoryId: 1
      }
    }

    const state = {
      cart: [

      ]
    }

    const data = reducer(state, action);
    const expectedState = {
      cart: [
        {
          id: 1,
          name: '',
          price: 100,
          description: '',
          categoryId: 1,
          amount: 1
        }
      ]
    }

    expect(data).toEqual(expectedState);
  })

  it('lessItem_actionWithIdOfItemThatAmountInStateIs1_emptyCart', () => {
    const action = {
      type: actionTypes.LESS_ITEM,
      id: 1
    }

    const state = {
      cart: [
        {
          id: 1,
          name: '',
          price: 100,
          description: '',
          categoryId: 1,
          amount: 1
        }
      ]
    }

    const data = reducer(state, action);
    const expectedState = {
      cart: []
    }

    expect(data).toEqual(expectedState)
  })

  it('lessItem_actionWithIdOfItemThatAmountInStateMore1_cartWithIdOfItemMinus1', () => {
    const action = {
      type: actionTypes.LESS_ITEM,
      id: 1
    }

    const state = {
      cart: [
        {
          id: 1,
          name: '',
          price: 100,
          description: '',
          categoryId: 1,
          amount: 2
        }
      ]
    }

    const data = reducer(state, action);
    const expectedState = {
      cart: [
        {
          id: 1,
          name: '',
          price: 100,
          description: '',
          categoryId: 1,
          amount: 1
        }
      ]
    }

    expect(data).toEqual(expectedState);
  })

  it('moreItem_actionWithId_cartWithItemOfIdAmountPlus1', () => {
    const action = {
      type: actionTypes.MORE_ITEM,
      id: 1
    }

    const state = {
      cart: [
        {
          id: 1,
          name: '',
          price: 100,
          description: '',
          categoryId: 1,
          amount: 1
        }
      ]
    }

    const data = reducer(state, action);
    const expectedState = {
      cart: [
        {
          id: 1,
          name: '',
          price: 100,
          description: '',
          categoryId: 1,
          amount: 2
        }
      ]
    }

    expect(data).toEqual(expectedState);
  })

  it('makeOrderStart_action_turnLoadingToTrue', () => {
    const action = {
      type: actionTypes.MAKE_ORDER_START
    }

    const data = reducer({}, action);
    const expectedState = {
      loading: true
    }

    expect(data).toEqual(expectedState);
  })

  it('makeOrderSuccess_action_turnLoadingToFalseCartToEmptyErrorToFalse', () => {
    const action = {
      type: actionTypes.MAKE_ORDER_SUCCESS
    }

    const data = reducer({}, action);
    const expectedState = {
      loading: false,
      cart: [],
      error: false
    }

    expect(data).toEqual(expectedState);
  })

  it('makeOrderFailed_action_turnLoadingToFalseTurnError', () => {
    const action = {
      type: actionTypes.MAKE_ORDER_FAILED,
      error: ''
    }

    const data = reducer({}, action);
    const expectedState = {
      loading: false,
      error: ''
    }

    expect(data).toEqual(expectedState);
  })

  it('clearCart_shouldClearCart_emptyArray', () => {
    const initialState = {
      cart: [
        {id: 1, name: 'Item1'},
        {id: 2, name: 'Item2'}
      ]
    }
    const act = {
      type: actionTypes.CLEAR_CART
    }
    const data = reducer(initialState, act);
    const expectedState = {
      cart: []
    };

    expect(data).toEqual(expectedState);
  })
})

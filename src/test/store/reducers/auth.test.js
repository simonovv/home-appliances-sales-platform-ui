import reducer from '../../../store/reducers/auth';
import * as actionTypes from '../../../store/actions/actionTypes';

describe('AuthReducerTest', () => {
  it('initialState', () => {
    const action = {}
    const data = reducer(undefined, action)
    const expectedState = {
      error: null,
      isAuth: true,
      loading: false,
      role: 0,
      token: null,
      userId: null
    }

    expect(data).toEqual(expectedState)
  })

  it('authStart_action_turnLoadingPropertyToTrue', () => {
    const action = {
      type: actionTypes.AUTH_START
    }
    const data = reducer({}, action);
    const expectedState = {
      loading: true
    }
    expect(data).toEqual(expectedState);
  })

  it('authFailed_action_turnLoadingToFalseAndIsAuthInTrue', () => {
    const action = {
      type: actionTypes.AUTH_FAIL,
      error: ''
    }
    const data = reducer({}, action);
    const expectedState = {
      loading: false,
      error: '',
      isAuth: false
    }

    expect(data).toEqual(expectedState);
  })

  it('authSuccess_action_turnLoadingToFalseIsAuthToTrueAndSetTokenUserIdRole', () => {
    const action = {
      type: actionTypes.AUTH_SUCCESS,
      token: '',
      userId: '',
      role: ''
    }
    const data = reducer({}, action);
    const expectedState = {
      loading: false,
      token: '',
      role: '',
      userId: '',
      isAuth: true
    }
    
    expect(data).toEqual(expectedState);
  })

  it('logout_action_turnIsAuthToFalseAndSetTokenUserIdRoleToNull', () => {
    const action = {
      type: actionTypes.AUTH_LOGOUT
    }
    const data = reducer({}, action);
    const expectedState = {
      isAuth: false,
      role: null,
      token: null,
      userId: null
    }
    
    expect(data).toEqual(expectedState)
  })

  it('deleteErrorMsg_action_turnErrorToNull', () => {
    const action = {
      type: actionTypes.DELETE_ERROR_MSG
    }
    const data = reducer({}, action);
    const expectedState = {
      error: null
    }
    
    expect(data).toEqual(expectedState);
  })
})
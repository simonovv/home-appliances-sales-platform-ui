import * as actionTypes from '../actions/actionTypes';

const initialState = {
    categories: null,
    loading: false,
    error: null,
    refresh: false
}

const fetchCategoryStart = (state, action) => {
    return {
        ...state,
        loading: true
    }
}

const fetchCategoryFailed = (state, action) => {
    return {
        ...state,
        error: action.error,
        loading: false
    }
}

const fetchCategorySucccess = (state, action) => {
    return {
        ...state,
        categories: action.categories,
        loading: false
    }
}

const createCategorySuccess = (state, action) => {
    return {
        ...state,
        categories: [...state.categories].concat(action.category),
        loading: false
    }
}

const createCategoryFailed = (state, action) => {
    return {
        ...state,
        loading: false,
        error: action.error
    }
}

const createCategoryStart = (state, action) => {
    return {
        ...state,
        loading: true
    }
}

const updateCategoryStart = (state, action) => {
    return {
        ...state,
        loading: true
    }
}

const updateCategorySuccess = (state, action) => {
    return {
        ...state,
        refresh: true,
        loading: false
    }
}

const updateCategoryFailed = (state, action) => {
    return {
        ...state,
        loading: false, 
        error: action.error
    }
}

const deleteCategoryStart = (state, action) => {
    return {
        ...state,
        loading: true
    }
}

const deleteCategorySuccess = (state, action) => {
    return {
        ...state,
        loading: false
    }
}

const deleteCategoryFailed = (state, action) => {
    return {
        ...state,
        loading: false,
        error: action.error
    }
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_CATEGORY_START: return fetchCategoryStart(state, action);
        case actionTypes.FETCH_CATEGORY_FAILED: return fetchCategoryFailed(state, action);
        case actionTypes.FETCH_CATEGORY_SUCCESS: return fetchCategorySucccess(state, action);
        case actionTypes.CREATE_CATEGORY_START: return createCategoryStart(state, action);
        case actionTypes.CREATE_CATEGORY_SUCCESS: return createCategorySuccess(state, action);
        case actionTypes.CREATE_CATEGORY_FAILED: return createCategoryFailed(state, action);
        case actionTypes.UPDATE_CATEGORY_START: return updateCategoryStart(state, action);
        case actionTypes.UPDATE_CATEGORY_SUCCESS: return updateCategorySuccess(state, action);
        case actionTypes.UPDATE_CATEGORY_FAILED: return updateCategoryFailed(state, action);
        case actionTypes.DELETE_CATEGORY_START: return deleteCategoryStart(state, action);
        case actionTypes.DELETE_CATEGORY_SUCCESS: return deleteCategorySuccess(state, action);
        case actionTypes.DELETE_CATEGORY_FAILED: return deleteCategoryFailed(state, action);
        default: return state;
    }
}

export default reducer;
import * as actionTypes from '../actions/actionTypes';

const intitialState = {
    error: false,
    loading: false
}

const regStart = (state, action) => {
    return {
        ...state,
        loading: true
    }
}

const regSuccess = (state, action) => {
    return {
        ...state,
        loading: false
    }
}

const regFailed = (state, action) => {
    return {
        ...state,
        error: action.error,
        loading: false
    }
}


const reducer = (state = intitialState, action) => {
    switch(action.type) {
        case actionTypes.REG_START: return regStart(state, action);
        case actionTypes.REG_SUCCESS: return regSuccess(state, action);
        case actionTypes.REG_FAILED: return regFailed(state, action);
        default: return state;
    }
}

export default reducer;
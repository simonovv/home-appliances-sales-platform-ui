import * as actionTypes from '../actions/actionTypes';

const initialState = {
  cart: [],
  loading: false, 
  error: null
}

const findIndex = (store, elementId) => {
  return(store.findIndex(e => {
      return e.id === elementId
  }))
}

const addNewItemToCart = (state, action) => {
  const oldState = {
    ...state
  }

  const index = findIndex(oldState.cart, action.item.id)
  
  if(index === -1) {
    action.item.amount = 1;
    return { 
      ...state,
      cart: [...state.cart, action.item]
    }
  }
  else {
    const cart = oldState.cart;
    cart[index].amount ++;
    return {
      ...state,
      cart: [...cart]
    }
  }
}

const clearCart = (state, action) => {
  return {
    ...state,
    cart: []
  }
}

const lessItem = (state, action) => {
  const oldState = {
    ...state
  }

  const index = findIndex(oldState.cart, action.id);
  const cart = oldState.cart;
  if(cart[index].amount !== 1) {
    cart[index].amount --;
  }
  else {
    cart.splice(index, 1);
  }
  return {
    ...state,
    cart: [...cart]
  
  }
}

const moreItem = (state, action) => {
  const oldState = {
    ...state
  }

  const index = findIndex(oldState.cart, action.id);

  const cart = oldState.cart;
  cart[index].amount ++;
  return {
    ...state,
    cart: [...cart]
  }
}

const makeOrderStart = (state, action) => {
  return {
    ...state,
    loading: true
  }
}

const makeOrderSuccess = (state, action) => {
  return {
    ...state, 
    loading: false,
    cart: [],
    error: false
  }
}

const makeOrderFailed = (state, action) => {
  return {
    ...state,
    loading: false, 
    error: action.error
  }
}

const reducer = (state = initialState, action) => {
  switch(action.type) {
    case actionTypes.ADD_NEW_ITEM_TO_CART: return addNewItemToCart(state, action);
    case actionTypes.LESS_ITEM: return lessItem(state, action);
    case actionTypes.MORE_ITEM: return moreItem(state, action);
    case actionTypes.MAKE_ORDER_START: return makeOrderStart(state, action);
    case actionTypes.MAKE_ORDER_SUCCESS: return makeOrderSuccess(state, action);
    case actionTypes.MAKE_ORDER_FAILED: return makeOrderFailed(state, action);
    case actionTypes.CLEAR_CART: return clearCart(state, action);
    default: return state;
  }
}

export default reducer;
import auth from './auth';
import category from './categories';
import reg from './registration';
import items from './items';
import cart from './cart';

export default {
    auth,
    category, 
    reg,
    items,
    cart
}
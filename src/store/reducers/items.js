import * as actionTypes from '../actions/actionTypes';

const initialState = {
    loading: false, 
    items: null,
    error: null,
    item: null
}

const fetchAllItemsSuccess = (state, action) => {
    return {
        ...state,
        loading: false,
        items: action.items
    }
}

const fetchAllItemsFailed = (state, action) => {
    return {
        ...state,
        error: action.error,
        loading: false
    }
}

const fetchAllItemsStart = (state, action) => {
    return {
        ...state, 
        loading: true
    }
}

const fetchSingleItemSuccess = (state, action) => {
    return {
        ...state,
        loading: false, 
        item: action.item
    }
}

const fetchSingleItemFailed = (state, action) => {
    return {
        ...state,
        loading: false,
        error: action.error
    }
}

const fetchSingleItemStart = (state, action) => {
    return {
        ...state,
        loading: true
    }
}

const createItemSuccess = (state, action) => {
    console.log(state);
    return {
        ...state,
        items: [...state.items].concat(action.item),
        loading: false
    }
}

const createItemStart = (state, action) => {
    return {
        ...state,
        loading: true
    }
}

const createItemFailed = (state, action) => {
    return {
        ...state,
        error: action.error,
        loading: false
    }
}

const updateItemSuccess = (state, action) => {
    return {
        ...state,
        loading: false
    }
}

const updateItemStart = (state, action) => {
    return {
        ...state,
        loading: true
    }
}

const updateItenFailed = (state, action) => {
    return {
        ...state, 
        error: action.error,
        loading: false
    }
}

const deleteItemStart = (state, action) => {
    return {
        ...state,
        loading: true
    }
}

const deleteItemFailed = (state, action) => {
    return {
        ...state, 
        loading: false,
        error: action.error
    }
}

const deleteItemSuccess = (state, action) => {
    return {
        ...state, 
        loading: false
    }
}

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.FETCH_ALL_ITEMS_START: return fetchAllItemsStart(state, action);
        case actionTypes.FETCH_ALL_ITEMS_FAILED: return fetchAllItemsFailed(state, action);
        case actionTypes.FETCH_ALL_ITEMS_SUCCESS: return fetchAllItemsSuccess(state, action);
        case actionTypes.FETCH_SINGLE_ITEM_SUCCESS: return fetchSingleItemSuccess(state, action);
        case actionTypes.FETCH_SINGLE_ITEM_START: return fetchSingleItemStart(state, action);
        case actionTypes.FETCH_SINGLE_ITEM_FAILED: return fetchSingleItemFailed(state, action);
        case actionTypes.CREATE_ITEM_SUCCESS: return createItemSuccess(state, action);
        case actionTypes.CREATE_ITEM_START: return createItemStart(state, action);
        case actionTypes.CREATE_ITEM_FAILED: return createItemFailed(state, action);
        case actionTypes.UPDATE_ITEM_SUCCESS: return updateItemSuccess(state, action);
        case actionTypes.UPDATE_ITEM_START: return updateItemStart(state, action);
        case actionTypes.UPDATE_ITEM_FAILED: return updateItenFailed(state, action);
        case actionTypes.DELETE_ITEM_START: return deleteItemStart(state, action);
        case actionTypes.DELETE_ITEM_SUCCESS: return deleteItemSuccess(state, action);
        case actionTypes.DELETE_ITEM_FAILED: return deleteItemFailed(state, action);
        default: return state;
    }
}

export default reducer;
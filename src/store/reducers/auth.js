import * as actionTypes from '../actions/actionTypes';

const initialState = {
    isAuth: false, //TEST MAKE FALSE FOR PROD
    token: null,
    userId: null,
    loading: false,
    role: null, //TEST MAKE NULL FOR PROD
    error: null
}

const authStart = (state, action) => {
    return {
        ...state,
        loading: true
    }
}

const authFail = (state, action) => {
    return {
        ...state,
        error: action.error,
        loading: false,
        isAuth: false
    }
}

const authSuccess = (state, action) => {
    return {
        ...state,
        loading: false,
        isAuth: true,
        userId: action.userId,
        token: action.token,
        role: action.role
    }
}

const logout = (state, action) => {
    return {
        isAuth: false,
        token: null,
        userId: null,
        role: null
    }
}

const deleteErrorMsg = (state, action) => {
    return {
        ...state,
        error: null
    }
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.AUTH_START: return authStart(state, action)
        case actionTypes.AUTH_FAIL: return authFail(state, action)
        case actionTypes.AUTH_SUCCESS: return authSuccess(state, action)
        case actionTypes.AUTH_LOGOUT: return logout(state, action)
        case actionTypes.DELETE_ERROR_MSG: return deleteErrorMsg(state, action)
        default: return state;
    }
}

export default reducer;
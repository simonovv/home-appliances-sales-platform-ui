import * as actionTypes from './actionTypes';

import axios from 'axios';

const fetchAllItemsSuccess = (items) => {
    return {
        type: actionTypes.FETCH_ALL_ITEMS_SUCCESS,
        items: items
    }
}

const fetchAllItemsFailed = (error) => {
    return {
        type: actionTypes.FETCH_ALL_ITEMS_FAILED,
        error: error
    }
}

const fetchAllItemsStart = () => {
    return {
        type: actionTypes.FETCH_ALL_ITEMS_START
    }
}

const fetchSingleItemSuccess = item => {
    return {
        type: actionTypes.FETCH_SINGLE_ITEM_SUCCESS,
        item: item
    }
}

const fetchSingleItemStart = () => {
    return {
        type: actionTypes.FETCH_SINGLE_ITEM_START
    }
}

const fetchSingleItemFailed = err => {
    return {
        type: actionTypes.FETCH_SINGLE_ITEM_FAILED,
        error: err
    }
}

const createItemSuccess = (item) => {
    return {
        type: actionTypes.CREATE_ITEM_SUCCESS,
        item: item
    }
}

const createItemFailed = err => {
    return {
        type: actionTypes.CREATE_ITEM_FAILED,
        error: err
    }
}

const createItemStart = () => {
    return {
        type: actionTypes.CREATE_ITEM_START
    }
}

const deleteItemStart = () => {
    return {
        type: actionTypes.DELETE_ITEM_START
    }
}

const deleteItemSuccess = () => {
    return {
        type: actionTypes.DELETE_ITEM_SUCCESS
    }
}

const deleteItemFailed = err => {
    return {
        type: actionTypes.DELETE_ITEM_FAILED,
        error: err
    }
}

const updateItemSuccess = (item) => {
    return {
        type: actionTypes.UPDATE_ITEM_SUCCESS,
        item: item
    }
}

const updateItemStart = () => {
    return {
        type: actionTypes.UPDATE_ITEM_START
    }
}

const updateItemFailed = err => {
    return {
        type: actionTypes.UPDATE_ITEM_FAILED,
        error: err
    }
}

export const fetchAllItems = (params) => {
    return dispatch => {
        console.log(params);

        dispatch(fetchAllItemsStart());
        const sortParams = !params ? '' : `?sort=${params.sort}`

        console.log(`http://localhost:3001/api/items${sortParams}`);

        axios.get(`http://localhost:3001/api/items${sortParams}`)
            .then(i => {
                dispatch(fetchAllItemsSuccess(i.data.items))
            })
            .catch(err => {
                console.log(err);
                dispatch(fetchAllItemsFailed(err.response.data.error))
            })
    }
}

export const fetchItemsByCategory = (id) => {
    return dispatch => {
        dispatch(fetchAllItemsStart());

        axios.get('http://localhost:3001/api/items/' + id)
            .then(i => {
                dispatch(fetchAllItemsSuccess(i.data.items));
            })
            .catch(err => {
                dispatch(fetchAllItemsFailed(err.response.data.error))
            }) 
    }
}

export const createNewItem = (name, category, description, price, img) => {
    return dispatch => {
        dispatch(createItemStart());

        let newItem = {
            name: name, 
            category: category,
            description: description,
            price: price
        }

        if(img) {
            newItem.img = img;
        }

        console.log('Creating new item');

        axios.post('http://localhost:3001/api/item', newItem)
            .then(res => {
                dispatch(createItemSuccess(res.data.item))
            })
            .catch(err => {
                console.log(err);
                dispatch(createItemFailed(err.response.data.error))
            })
    }
}

export const editItem = (id, category, name, description, price, img, history) => {
    return dispatch => {
        dispatch(updateItemStart())

        let newItem = {
            id: id,
            category: category,
            name: name, 
            description: description,
            price: price,
            img: img
        }

        axios.post('http://localhost:3001/api/updateItem', newItem)
            .then(res => {
                dispatch(updateItemSuccess(res.data.item))
                history.push('/refresh/');
            })
            .catch(err => {
                dispatch(updateItemFailed(err.response.data.error))
            })
    }
}

export const fetchItemById = (id) => {
    return dispatch => {
        dispatch(fetchSingleItemStart());

        axios.get('http://localhost:3001/api/item/' + id)
            .then(i => {
                console.log(i);
                dispatch(fetchSingleItemSuccess(i.data.item));
            })
            .catch(err => {
                dispatch(fetchSingleItemFailed(err.response.data.error))
            })
    }
}

export const deleteItem = (id, history) => {
    return dispatch => {
        dispatch(deleteItemStart())

        axios.post(`http://localhost:3001/api/item/${id}`)
            .then(() => {
                dispatch(deleteItemSuccess());
                history.push('/refresh/');
            })
            .catch(err => {
                console.log('ERR')
                dispatch(deleteItemFailed(err.response.data.error))
            })
    }
}
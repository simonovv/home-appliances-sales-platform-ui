import * as actionTypes from './actionTypes';

import axios from 'axios';

export const addItemToCart = (item) => {
  return {
    type: actionTypes.ADD_NEW_ITEM_TO_CART,
    item: item
  }
}

export const lessItem = (id) => {
  return {
      type: actionTypes.LESS_ITEM,
      id: id
  }
}

export const moreItem = (id) => {
  return {
      type: actionTypes.MORE_ITEM,
      id: id
  }
}

const makeOrderStart = () => {
  return {
    type: actionTypes.MAKE_ORDER_START
  }
}

const makeOrderSuccess = () => {
  return {
    type: actionTypes.MAKE_ORDER_SUCCESS
  }
}

const makeOrderFailed = (err) => {
  return {
    type: actionTypes.MAKE_ORDER_FAILED,
    error: err
  }
}

export const clearCart = () => {
  return {
    type: actionTypes.CLEAR_CART
  }
}

export const makeOrder = (name, surname, email, phone, items, details, history) => {
  return dispatch => {
    dispatch(makeOrderStart());

    const data = {
      name,
      surname,
      email,
      phone,
      items,
    }

    if(details) data.details = details;

    axios.post('http://localhost:3001/api/order', data)
      .then(() => {
        dispatch(makeOrderSuccess());
        history.push('/');
      })
      .catch(err => {
        dispatch(makeOrderFailed(err.response.data.error))
      })
  }
}
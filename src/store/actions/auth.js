import axios from 'axios';

import * as actionTypes from './actionTypes';

export const authStart = () => {
    return {
        type: actionTypes.AUTH_START
    }
}

export const authSuccess = (token, userId, role) => {
    return {
        type: actionTypes.AUTH_SUCCESS,
        token: token,
        userId: userId,
        role: role
    }
}

export const authFailed = (err) => {
    return {
        type: actionTypes.AUTH_FAIL,
        error: err
    }
}

export const logout = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('userId');
    return {
        type: actionTypes.AUTH_LOGOUT
    }
}

export const deleteErrorMsg = () => {
    return {
        type: actionTypes.DELETE_ERROR_MSG
    }
}

export const auth = (email, password, history) => {
    return dispatch => {
        dispatch(authStart());
        const authData = {
            email: email,
            password: password
        }

        axios.post('http://localhost:3001/api/login', authData)
            .then(res => {
                console.log(res);
                dispatch(authSuccess(res.data.user.token, res.data.user.userId, res.data.user.role));
                localStorage.setItem('token', res.token);
                localStorage.setItem('userId', res.userId);
                history.push('/');
            })
            .catch(err => {
                dispatch(authFailed(err.response.data.error))
            })
    }
}

export default auth;
import * as actionTypes from './actionTypes';

import axios from 'axios';

const fetchCategoriesStart = () => {
    return {
        type: actionTypes.FETCH_CATEGORY_START,
    }
}

const fetchCategoriesSucccess = (categories) => {
    return {
        type: actionTypes.FETCH_CATEGORY_SUCCESS,
        categories: categories
    }
}

const fetchCategoriesFailed = (err) => {
    return {
        type: actionTypes.FETCH_CATEGORY_FAILED,
        error: err
    }
}

const createCategoryStart = () => {
    return {
        type: actionTypes.CREATE_CATEGORY_START
    }
}

const createCategorySuccess = (category) => {
    return {
        type: actionTypes.CREATE_CATEGORY_SUCCESS,
        category
    }
}

const createCategoryFailed = err => {
    return {
        type: actionTypes.CREATE_CATEGORY_FAILED,
        error: err
    }
}

const updateCategoryStart = () => {
    return {
        type: actionTypes.UPDATE_CATEGORY_START
    }
}

const updateCategorySuccess = cat => {
    return {
        type: actionTypes.UPDATE_CATEGORY_SUCCESS,
        category: cat
    }
}

const updateCategoryFailed = err => {
    return {
        type: actionTypes.UPDATE_CATEGORY_FAILED,
        error: err
    }
}

const deleteCategoryStart = () => {
    return {
        type: actionTypes.DELETE_CATEGORY_START
    }
}

const deleteCategorySuccess = () => {
    return {
        type: actionTypes.DELETE_CATEGORY_SUCCESS
    }
}

const deleteCategoryFailed = (err) => {
    return {
        type: actionTypes.DELETE_CATEGORY_FAILED,
        error: err
    }
}

export const createCategory = (name, img, handler) => {
    return dispatch => {
        dispatch(createCategoryStart());

        const newCategory = {
            name
        }

        if(img) {
            newCategory.img = img;
        }

        axios.post('http://localhost:3001/api/category', newCategory)
            .then(res => {
                dispatch(createCategorySuccess(res.data.category))
                handler()
            })
            .catch(err => {
                console.log(err.response.data.error);
                dispatch(createCategoryFailed(err.response.data.error))
            }) 
    }
}

export const fetchAllCategories = () => {
    return dispatch => {
        dispatch(fetchCategoriesStart());

        axios.get('http://localhost:3001/api/category')
            .then(res => {
                console.log(res.data);
                dispatch(fetchCategoriesSucccess(res.data.categories))
            })
            .catch(err => {
                dispatch(fetchCategoriesFailed(err.response.data.error))
            })

    }
}

export const updateCategory = (id ,name, img, history) => {
    return dispatch => {
        dispatch(updateCategoryStart())

        const newCategory = {
            name,
            img
        };

        axios.post(`http://localhost:3001/api/category/${id}`, newCategory)
            .then(res => {
                dispatch(updateCategorySuccess(res.data.category));
                history.push(`/refresh/category`);
            })
            .catch(err => {
                dispatch(updateCategoryFailed(err.response.data.error))
            })
    }
}

export const deleteCategory = (id, history) => {
    return dispatch => {
        dispatch(deleteCategoryStart());

        axios.post(`http://localhost:3001/api/category/delete/${id}`)
            .then(() => {
                dispatch(deleteCategorySuccess());
                history.push('/refresh/category');
            })
            .catch(err => {
                dispatch(deleteCategoryFailed(err.response.data.error))
            })
    }
}
import axios from 'axios';

import * as actionTypes from './actionTypes';

const regStart = () => {
    return {
        type: actionTypes.REG_START
    }
}

const regSuccess = () => {
    return {
        type: actionTypes.REG_SUCCESS
    }
}

const regFailed = (err) => {
    return {
        type: actionTypes.REG_FAILED,
        error: err
    }
}

const reg = (email, password, handler) => {
    return dispatch => {
        const regData = {
            email: email,
            password: password
        }

        dispatch(regStart());
        axios.post('http://localhost:3001/api/register', regData)
            .then(res => {
                dispatch(regSuccess())
                handler();
            })
            .catch(err => {
                console.log(err);
                dispatch(regFailed(err.response.data.error))
            })
    }
}

export default reg;
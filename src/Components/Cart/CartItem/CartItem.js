import React from "react";
import { Link } from "react-router-dom";

import lessIcon from '../../../assets/less-icon.png';
import moreIcon from '../../../assets/more-icon.png';

import classes from "./CartItem.css";

const cartItem = props => {
  const { id, img, name, price, amount, moreHandler, lessHandler } = props;
  return (
    <Link className={classes.link} to={`/item/${id}`}>
      <div className={classes.item}>
        <img alt="img" src={img} className={classes.img} />
        <p className={classes.name}>{name}</p>
        <p>{price}$</p>
        <div className={classes.amountWrapper}>
          <img src={lessIcon} alt='less' onClick={(event) => lessHandler(event, id)}/>
          <p>{amount}</p>
          <img src={moreIcon} alt='more' onClick={(event) => moreHandler(event, id)}/>
        </div>
        <p className={classes.cost}>{amount * price}$</p>
      </div>
    </Link>
  );
};

export default cartItem;

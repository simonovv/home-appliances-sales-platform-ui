import React from 'react';

import classes from './Item.css';

import editIcon from '../../assets/edit-icon.png';

import Aux from '../../hoc/Aux/Aux';

const item = (props) => {
    return(
        <Aux>
            <div className={classes.item} onClick={props.selectHandler}>
            {props.role === 0 && <img onClick={props.editHandler} className={classes.edit} src={editIcon} alt='Edit'/>}
            <div className={classes.imgWrapper}>
                <img className={classes.img} src={props.img} alt='Item img'/>
            </div>
            <div className={classes.contentWrapper}>
            <p className={classes.name}>{props.name}</p>
            <div className={classes.descriptionWrapper}>
                <p className={classes.description}>{props.description}</p>
            </div>
            <p className={classes.price}>{props.price + '$'}</p>
            </div>
            </div>
        </Aux>
    )
}

export default item;
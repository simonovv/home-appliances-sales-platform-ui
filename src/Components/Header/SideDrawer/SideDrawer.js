import React from 'react'
import Aux from '../../../hoc/Aux/Aux'
import NavigationItems from '../NavigationItems/NavigationsItems';
import Backdrop from '../../UI/Backdrop/Backdrop'

import classes from './SideDrawer.css'

const sideDrawer = (props) => {
    let attachedClases = [classes.SideDrawer, classes.Close];
    if(props.open) {
        attachedClases = [classes.SideDrawer, classes.Open]
    }

    return(
        <Aux>
            <Backdrop show={props.show} clicked={props.clicked}/>
            <div className={attachedClases.join(' ')}>
                <nav className={classes.DrawerItemsWrapper}>
                    <NavigationItems list={props.list}/>
                </nav>
            </div>
        </Aux>
    );
}

export default sideDrawer;
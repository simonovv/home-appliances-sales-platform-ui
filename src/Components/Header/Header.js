import React, { Component } from 'react';

import classes from './Header.css'

import NavigationItems from './NavigationItems/NavigationsItems'
import DrawerToggle from './SideDrawer/DrawerToggle/DrawerToggle'
import SideDrawer from './SideDrawer/SideDrawer'

import { connect } from 'react-redux'

import { withRouter } from 'react-router-dom';

class Header extends Component {
    state = {
        draweOpend: false
    }

    drawerOpenHandler = () => {
        this.setState({draweOpend: true})
    }

    drawerCloseHandler = () => {
        this.setState({draweOpend: false})
    }

    guest = [
        {name: 'Home', link: '/', exact: true},
        {name: 'Category', link: '/category', exact: false},
        {name: 'Auth', link: '/auth', exact: false}
    ]

    user = [
        {name: 'Home', link: '/', exact: true},
        {name: 'Category', link: '/category', exact: false},
        {name: 'Logout', link: '/logout', exact: true}
    ]

    render() {
        return(
            <header className={classes.header}>
                <nav className={classes.DesktopOnly}>
                    <NavigationItems list={this.props.isAuth ? this.user : this.guest }/>
                </nav>
                <DrawerToggle clicked={this.drawerOpenHandler}/>
                <SideDrawer list={this.props.isAuth ? this.user : this.guest} open={this.state.draweOpend} clicked={this.drawerCloseHandler} show={this.state.draweOpend}/>
            </header>
        );
    }
}

const mapStateToProps = state => {
    return {
        isAuth: state.auth.isAuth
    }
}

export default withRouter(connect(mapStateToProps, null)(Header));
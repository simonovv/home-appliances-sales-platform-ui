import React from "react";

import classes from "./NavigationItems.css";

import NavigationItem from "./NavigationItem/NavigationItem";

const navigationItems = props => {
    return (
        <ul className={classes.navigationItems} style={props.style}>
            {props.list.map(element => (
                <NavigationItem
                    key={element.name} 
                    link={element.link} 
                    exact={element.exact}>
                    {element.name}
                </NavigationItem>
            ))}
        </ul>
    );
};

export default navigationItems;

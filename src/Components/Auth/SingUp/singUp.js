import React from 'react'
import Input from '../../UI/Input/Input'
import Aux from '../../../hoc/Aux/Aux';

const singUp = (props) => {
    return(
        <Aux>
            {props.form.map((element, i) => {
                return <Input
                    autoFocus={i === 0} 
                    key={element.id} 
                    elementType={element.config.elementType} 
                    elementConfig={element.config.elementConfig}
                    value={props.form.value}
                    touched={element.config.touched}
                    shouldValidate={element.config.validationRules}
                    valid={!element.config.valid}
                    typeHandle={(event) => props.typeHandle(event, element.config.elementConfig.id)}
                    />
            })}
        </Aux>
    );
}

export default singUp;
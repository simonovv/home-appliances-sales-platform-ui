import React from 'react';

import classes from './Category.css';

import editingIcon from '../../assets/edit-icon.png';

const category = (props) => {
    return(
        <div onClick={props.selectHandler}
            className={classes.category} 
            style={{backgroundImage: `url(${props.img})`}}>
            {props.role === 0 && <img alt='edit' onClick={props.editHandler} className={classes.edit} src={editingIcon}/>}
            <p>{props.name}</p>
        </div>
    );
}

export default category;
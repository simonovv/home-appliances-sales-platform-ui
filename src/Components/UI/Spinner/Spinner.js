import React from 'react';

import classes from './Spinner.css';

const spinner = () => (
    <div className={classes.ldsRolles}><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
)

export default spinner;
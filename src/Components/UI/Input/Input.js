import React from 'react'

import classes from './Input.css'

const input = (props) => {
    let input = null;
    const inputClasses = props.elementType === 'input' ? [classes.input] : [classes.textarea];

    if(props.valid && props.touched) {
        inputClasses.push(classes.invalid);
    }

    switch(props.elementType){
        case 'input': 
            input =  <input 
                autoFocus={props.autoFocus}
                className={inputClasses.join(' ')} 
                value={props.value} 
                onChange={props.typeHandle}
                {...props.elementConfig}
                name={props.name}/>
                break;
        case 'textarea': 
            input =  <textarea
                className={inputClasses.join(' ')}
                value={props.value}
                onChange={props.typeHandle}
                name={props.name} 
                {...props.elementConfig}/>
            break;
        case 'select': {
            input = (
                <select className={classes.select} onChange={props.changeHandler} defaultValue={props.defaultValue}>
                    <option value="" disabled>Select category</option>
                    {props.list.map(ell => {
                        return <option onChange={props.changeHandler} key={ell.id} value={ell.id} id={ell.id}>{ell.name}</option>
                    })}
                </select>
            )
            break;
                }
        default: 
            input =  <input {...props.elementConfig}/>
    }

    return input;
}

export default input;
import React, { Component } from "react";
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux'

import sortByASC from '../../assets/sort-a-z.png';
import sortByDesc from '../../assets/sort-z-a.png';
import newItem from '../../assets/new-item.png';

import { fetchAllItems, fetchItemsByCategory } from '../../store/actions/items';

import classes from './items.css';

import Item from '../../Components/Item/Item';
import CreateItem from './CreateItem/CreateItem';
import EditingItem from './EditingItem/EditingItem';

export class Items extends Component {
    componentDidMount() {
        if(process.env.NODE_ENV !== 'test') {
        this.props.match.params.id ?
            this.props.fetchItemsByCategory(this.props.match.params.id) :
                this.props.fetchAllItems();
        }
    }

    state = {
        items: null,
        creatingNewItem: false,
    }

    editHandler = (event, element) => {
        event.stopPropagation();

        let newItems = this.state.items ? [...this.state.items] : [...this.props.items];

        newItems[this.findIndex(element)].isEditing = true;

        this.setState({items: newItems})
    }

    findIndex = (element) => {
        return(this.props.items.findIndex(e => {
            return e.name === element
        }))
    }

    closeHandler = () => {
        this.setState({creatingNewItem: false});
    }

    sortByASC = () => {
        this.props.fetchAllItems({sort: 'ASC'})
    }

    sortByDesc = () => {
        this.props.fetchAllItems({sort: 'DESC'})
    }

    selectItemHandler = (event, id) => {
        this.props.history.push('/item/' + id);
    }

    searchHandler = (event) => {
        let reg = '(' + event.target.value + ')';

        let newItems = [];

        for (let item in this.props.items) {
            if (this.props.items[item].name.search(reg) !== -1) {
                newItems.push(this.props.items[item]);
            };
        }

        this.setState({ items: newItems })
    }

    createNewItemHandler = () => {
        this.setState({ creatingNewItem: true });
    }

    closeEditingHandler = (element) => {

        let newItems = this.state.items ? [...this.state.items] : [...this.props.items];

        newItems[this.findIndex(element)].isEditing = false;

        this.setState({items: newItems})
    }

    render() {
        const items = this.state.items || this.props.items;

        return (
            <div className={classes.itemsContainer}>
                <input className={classes.search} onKeyUp={(event) => this.searchHandler(event)} />
                <div className={classes.controls}>
                    {this.props.role === 1 &&
                        <img onClick={this.createNewItemHandler} src={newItem} alt='Crate new item' />}
                        <img onClick={this.sortByASC} src={sortByASC} alt='Sort by name ASC' />
                        <img onClick={this.sortByDesc} src={sortByDesc} alt='Sort by name DESC' />
                </div>
                {this.state.creatingNewItem && 
                    <CreateItem 
                        closeHandler={this.closeHandler} />}
                {items && items.map(element => {
                    if(element.isEditing) {
                        return <EditingItem 
                                    id={element.id}
                                    category={element.categoryId}
                                    closeHandler={() => this.closeEditingHandler(element.name)}
                                    key={element.id}
                                    name={element.name}
                                    description={element.description}
                                    price={element.price}
                                    img={element.img}/>
                    }
                    else return <Item
                                editHandler={(event) => this.editHandler(event, element.name)}
                                key={element.id}
                                name={element.name}
                                description={element.description}
                                price={element.price}
                                role={this.props.role}
                                selectHandler={(event) => this.selectItemHandler(event, element.id)}
                                img={element.img}
                                />
                    })}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        items: state.items.items,
        role: state.auth.role,
        refresh: state.items.refresh
    }
}

const mapDispatchToProps = dispatch => {
    return {
        fetchAllItems: (params) => dispatch(fetchAllItems(params)),
        fetchItemsByCategory: (id) => dispatch(fetchItemsByCategory(id))
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Items));
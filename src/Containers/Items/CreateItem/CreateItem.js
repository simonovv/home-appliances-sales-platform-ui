import React, { Component } from "react";

import Dropzone from "react-dropzone";
import request from "superagent";
import isValid from "../../../utility/isValid";

import { connect } from "react-redux";
import { createNewItem } from "../../../store/actions/items";
import { withRouter } from "react-router-dom";
import { fetchAllCategories } from "../../../store/actions/categories";

import Input from "../../../Components/UI/Input/Input";

import closeIcon from "../../../assets/close-icon.png";

import classes from "./CreateItem.css";

const CLOUDINARY_UPLOAD_PRESET = "qim6uc9u";
const CLOUDINARY_UPLOAD_URL = "https://api.cloudinary.com/v1_1/simonov/upload";

class CreateItem extends Component {
  constructor(props) {
    super();

    this.state = {
      uploadedFileCloudinaryUrl: "",
      newItem: {
        name: {
          value: "",
          valid: false,
          touched: false,
          validation: {
            required: true,
            minLength: 3,
            maxLength: 25
          }
        },
        category: {
          value: ""
        },
        description: {
          value: "",
          valid: false,
          touched: false,
          validation: {
            minLength: 25,
            maxLength: 100
          }
        },
        price: {
          valid: false,
          touched: false,
          value: 0,
          validation: {
            isNumeric: true
          }
        }
      }
    };
  }

  componentDidMount() {
    this.props.fetchAllCategories();
  }

  typeHandler = (event, element) => {
    const updatedItem = {
      ...this.state.newItem,
      [element]: {
        ...this.state.newItem[element],
        value: event.target.value,
        valid: isValid(
          event.target.value,
          this.state.newItem[element].validation
        ),
        touched: true
      }
    };

    this.setState({ newItem: updatedItem });
  };

  selectChangeHandler = (event, element) => {
    console.log(event.target.value);
    const updatedItem = {
      ...this.state.newItem,
      [element]: {
        ...this.state.newItem[element],
        value: event.target.value
      }
    };
    console.log(updatedItem);

    this.setState({ newItem: updatedItem });
  };

  createNewItemHandler = () => {
    this.props.createNewItem(
      this.state.newItem.name.value,
      this.state.newItem.category.value,
      this.state.newItem.description.value,
      this.state.newItem.price.value,
      this.state.uploadedFileCloudinaryUrl
    );
    this.props.history.push("/refresh/");
  };

  dropzoneDropHandler = file => {
    this.setState({
      uploadedFile: file[0]
    });
    this.handleImageUpload(file[0]);
  };

  checkClass = element => {
    if (
      !this.state.newItem[element].valid &&
      this.state.newItem[element].touched
    ) {
      return [classes[element], classes.invalid].join(" ");
    }
    return classes[element];
  };

  handleImageUpload = file => {
    let upload = request
      .post(CLOUDINARY_UPLOAD_URL)
      .field("upload_preset", CLOUDINARY_UPLOAD_PRESET)
      .field("file", file);

    upload.end((err, response) => {
      if (err) {
        console.error(err);
      }

      if (response.body.secure_url !== "") {
        this.setState({
          uploadedFileCloudinaryUrl: response.body.secure_url
        });
      }
    });
  };

  render() {
    return (
      <div className={classes.item} onClick={this.props.selectHandler}>
        <img
          className={classes.closeButton}
          onClick={this.props.closeHandler}
          src={closeIcon}
          alt="Close button"
        />
        <div className={classes.imgWrapper}>
          {this.state.uploadedFileCloudinaryUrl ? (
            <img
              className={classes.uploadImg}
              src={this.state.uploadedFileCloudinaryUrl}
              alt="Uploaded img"
            />
          ) : (
            <Dropzone
              className={classes.dropzone}
              multiple={false}
              accept="image/*"
              onDrop={this.dropzoneDropHandler}
            >
              <p>Drop an image or click to select a file to upload</p>
            </Dropzone>
          )}
        </div>
        <div className={classes.contentWrapper}>
          <input
            onChange={event => this.typeHandler(event, "name")}
            placeholder="Name"
            className={this.checkClass("name")}
            autoFocus
          />
          <div className={classes.descriptionWrapper}>
            <textarea
              onChange={event => this.typeHandler(event, "description")}
              placeholder="Description"
              className={this.checkClass("description")}
            />
          </div>
          <div className={classes.priceSave}>
            <input
              onChange={event => this.typeHandler(event, "price")}
              placeholder="Price"
              className={this.checkClass("price")}
            />
            <button
              disabled={
                !(
                  this.state.newItem.name.valid &&
                  this.state.newItem.description.valid &&
                  this.state.newItem.price.valid &&
                  this.state.newItem.category.value !== ""
                )
              }
              className={classes.saveButton}
              onClick={this.createNewItemHandler}
            >
              Save
            </button>
          </div>
          <Input
              changeHandler={event =>
                this.selectChangeHandler(event, "category")
              }
              defaultValue={this.state.newItem.category.value}
              elementType="select"
              list={this.props.categories || []}
            />
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
    return {
        categories: state.cat.categories,
        error: state.cat.error
    }
}

const mapDispatchToProps = dispatch => {
  return {
    createNewItem: (name, category, description, price, img) =>
      dispatch(createNewItem(name, category, description, price, img)),
    fetchAllCategories: () => dispatch(fetchAllCategories())
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(CreateItem)
);

import React, { Component } from 'react';
import { connect } from 'react-redux';

import { fetchItemById } from '../../../store/actions/items';
import { addItemToCart } from '../../../store/actions/cart';

import classes from './FullItem.css';

class FullItem extends Component {
    componentDidMount() {
        this.props.fetchItemById(this.props.match.params.id);
    }

    addToCartHandler = () => {
        console.log(this.props.item);
        this.props.addToCart(this.props.item);
    }

    render() {
        return(
            <div>
                {this.props.item && 
                    <div className={classes.fullitemWrapper}>
                        <div className={classes.fullitem}>
                            <img 
                                className={classes.img} 
                                src={this.props.item.img}
                                alt='Item img'/>
                            <div className={classes.info}>
                                <div className={classes.infoprice}>
                                    <p className={classes.name}>{this.props.item.name}</p>
                                    <p className={classes.price}>{this.props.item.price + '$'}</p>
                                </div>
                                <p className={classes.description}>{this.props.item.description}</p>
                                <button className={classes.addToCart} onClick={this.addToCartHandler} disabled={!this.props.isAuth ? true : false}>Add To Cart</button>
                            </div>
                        </div>
                </div>}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        item: state.items.item,
        isAuth: state.auth.isAuth
    }
}

const mapDispatchToProps = dispatch => {
    return {
        fetchItemById: (id) => dispatch(fetchItemById(id)),
        addToCart: (item) => dispatch(addItemToCart(item))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FullItem);
import React, { Component } from 'react';

import Dropzone from 'react-dropzone';
import request from 'superagent';
import isValid from '../../../utility/isValid';

import { connect } from 'react-redux';
import { editItem, deleteItem } from '../../../store/actions/items';
import { withRouter } from 'react-router-dom';
import { fetchAllCategories } from '../../../store/actions/categories';

import Input from '../../../Components/UI/Input/Input';

import closeIcon from '../../../assets/close-icon.png';

import classes from './EditingItem.css';

const CLOUDINARY_UPLOAD_PRESET = 'qim6uc9u';
const CLOUDINARY_UPLOAD_URL = 'https://api.cloudinary.com/v1_1/simonov/upload';

class EditingItem extends Component {
    constructor(props) {
        super();

        this.state = {
            uploadedFileCloudinaryUrl: '',
            newItem: {
                id: {
                    value: props.id
                },
                category: {
                    value: props.category
                },
                name: {
                    value: props.name,
                    valid: true,
                    touched: false,
                    validation: {
                        required: true,
                        minLength: 3,
                        maxLength: 25
                    }
                },
                description: {
                    value: props.description,
                    valid: true,
                    touched: false,
                    validation: {
                        minLength: 25,
                        maxLength: 100
                    }
                },
                price: {
                    valid: true,
                    touched: false,
                    value: props.price,
                    validation: {
                        isNumeric: true
                    }
                },
                img: {
                    value: props.img
                }
            }
        };
    }

    componentDidMount() {
        this.props.fetchAllCategories();
    }

    typeHandler = (event, element) => {
        const updatedItem = {
            ...this.state.newItem,
            [element]: {
                ...this.state.newItem[element],
                value: event.target.value,
                valid: isValid(event.target.value, this.state.newItem[element].validation),
                touched: true
            }
        }

        this.setState({ newItem: updatedItem })
    }

    dropzoneDropHandler = (file) => {
        this.setState({
            uploadedFile: file[0]
        });
        this.handleImageUpload(file[0]);
    }

    checkClass = (element) => {
        if(!this.state.newItem[element].valid && this.state.newItem[element].touched) {
            return [classes[element], classes.invalid].join(' ');
        }
        return classes[element];
    }

    selectChangeHandler = (event, element) => {
        console.log(event.target.value)
        const updatedItem = {
            ...this.state.newItem,
            [element]: {
                ...this.state.newItem[element],
                value: event.target.value
            }
        }
        console.log(updatedItem);

        this.setState({ newItem: updatedItem })
    }

    handleImageUpload = (file) => {
        let upload = request.post(CLOUDINARY_UPLOAD_URL)
            .field('upload_preset', CLOUDINARY_UPLOAD_PRESET)
            .field('file', file);

        upload.end((err, response) => {
            if (err) {
                console.error(err);
            }

            if (response.body.secure_url !== '') {
                this.setState({
                    uploadedFileCloudinaryUrl: response.body.secure_url
                });
            }
        });
    }

    editItem = () => {
        this.props.editItem(this.state.newItem.id.value, this.state.newItem.category.value, this.state.newItem.name.value, this.state.newItem.description.value, this.state.newItem.price.value, this.state.newItem.img.value, this.props.history);
    }

    deleteItemHandler = () => {
        this.props.deleteItem(this.state.newItem.id.value, this.props.history)
    }

    render() {
        return (
            <div className={classes.item} onClick={this.props.selectHandler}>
                <img 
                    className={classes.closeButton} 
                    onClick={this.props.closeHandler} 
                    src={closeIcon} 
                    alt='Close button'/>
                <div className={classes.imgWrapper}>
                        <img 
                            className={classes.uploadImg} 
                            src={this.state.uploadedFileCloudinaryUrl ? this.state.uploadedFileCloudinaryUrl : this.state.newItem.img.value}
                            alt='Uploaded img'/>
                        <Dropzone
                            style={{position: 'absolute'}}
                            className={classes.dropzone}
                            multiple={false}
                            accept='image/*'
                            onDrop={this.dropzoneDropHandler}>
                        </Dropzone>
                </div>
                <div className={classes.contentWrapper}>
                    <input
                        onChange={(event) => this.typeHandler(event, 'name')}
                        placeholder='Name' 
                        className={this.checkClass('name')}
                        value={this.state.newItem.name.value} />
                    <div className={classes.descriptionWrapper}>
                        <textarea
                            onChange={(event) => this.typeHandler(event, 'description')} 
                            placeholder='Description' 
                            className={this.checkClass('description')}
                            value={this.state.newItem.description.value} />
                    </div>
                    <div className={classes.priceSave}>
                        <input 
                            onChange={(event) => this.typeHandler(event, 'price')} 
                            placeholder='Price' className={this.checkClass('price')} value={this.state.newItem.price.value}/>
                        <Input changeHandler={(event) => this.selectChangeHandler(event, 'category')} elementType='select' defaultValue={this.state.newItem.category.value} list={this.props.categories ? this.props.categories : []}/>
                        <button
                            onClick={this.deleteItemHandler}
                            className={classes.saveButton}>Delete</button>
                        <button 
                            disabled={!(this.state.newItem.name.valid && 
                                this.state.newItem.description.valid && 
                                    this.state.newItem.price.valid)} 
                            className={classes.saveButton} onClick={this.editItem}>Save</button>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        categories: state.cat.categories
    }
}

const mapDispatchToProps = dispatch => {
    return {
        editItem: (id, category, name, description, price, img, history) => dispatch(editItem(id, category, name, description, price, img, history)),
        fetchAllCategories: () => dispatch(fetchAllCategories()),
        deleteItem: (id, history) => dispatch(deleteItem(id, history))
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EditingItem));
import React, { Component } from "react";
import { connect } from 'react-redux';
import isValid from '../../utility/isValid';

import { makeOrder, clearCart } from '../../store/actions/cart';

import closeButton from '../../assets/close-icon.png';

import classes from './Order.css';

import Input from '../../Components/UI/Input/Input';

class Order extends Component {
  state = {
    form: {
      name: {
        name: 'name',
        elementType: "input",
        value: '',
        valid: false,
        touched: false,
        elementConfig: {
          type: "text",
          placeholder: "Your name"
        },
        validationRules: {
          required: true,
          minLength: 3,
          maxLength: 25
        }
      },
      surname: {
        name: 'surname',
        elementType: "input",
        value: '',
        valid: false,
        touched: false,
        elementConfig: {
          type: "text",
          placeholder: "Your surname"
        },
        validationRules: {
          required: true,
          minLength: 3,
          maxLength: 25
        }
      },
      email: {
        name: 'email',
        elementType: 'input',
        value: '',
        valid: false,
        touched: false,
        elementConfig: {
          type: "text",
          placeholder: 'Your e-mail'
        },
        validationRules: {
          required: true,
          minLength: 3,
          maxLength: 25,
          isEmail: true
        }
      },
      phoneNumber: {
        name: 'phoneNumber',
        elementType: 'input',
        value: '',
        valid: false, 
        touched: false,
        elementConfig: {
          type: 'text',
          placeholder: 'Your phone number'
        },
        validationRules: {
          required: true
        }
      },
      details: {
        name: 'details',
        elementType: 'textarea',
        value: '',
        valid: true,
        touched: false,
        elementConfig: {
          type: 'text',
          placeholder: 'Details'
        },
        validationRules: {
          maxLength: 1000
        }
      }
    }
  }

  typeHandler = (event) => {
    let updatedForm = {
      ...this.state.form,
      [event.target.name]: {
        ...this.state.form[event.target.name],
        value: event.target.value,
        valid: isValid(event.target.value, this.state.form[event.target.name].validationRules),
        touched: true
      }
    };
    this.setState({ form: updatedForm });
  };

  clearCart = () => {
    this.props.clearCart();
    this.props.history.push('/');
  }

  makeOrder = () => {
    const { form: { name, surname, phoneNumber, details, email } } = this.state;
    console.log(this.props.items);
    this.props.makeOrder(
      name.value,
      surname.value,
      email.value,
      phoneNumber.value,
      this.props.items,
      details.value,
      this.props.history
    )
  }

  render() {
    return (
      <div className={classes.formWrapper}>
        <div className={classes.form}>
          <img onClick={this.clearCart} className={classes.closeButton} alt='close' src={closeButton}/>
          <h1 className={classes.title}>Order</h1>
          <div className={classes.column}>
            <div className={classes.column}>
              <p>Name: </p>
              <Input 
                elementType={this.state.form.name.elementType} 
                name={this.state.form.name.name}
                typeHandle={this.typeHandler}
                elementConfig={this.state.form.name.elementConfig}
                valid={!this.state.form.name.valid}
                touched={this.state.form.name.touched}/>
            </div>
            <div className={classes.column}>
              <p>Surname: </p>
              <Input 
                elementType={this.state.form.surname.elementType} 
                name={this.state.form.surname.name}
                typeHandle={this.typeHandler}
                elementConfig={this.state.form.surname.elementConfig}
                valid={!this.state.form.surname.valid}
                touched={this.state.form.surname.touched}/>
            </div>
          </div>
          <div className={classes.column}>
            <div className={classes.column}>
              <p>E-mail: </p>
              <Input 
                elementType={this.state.form.email.elementType} 
                name={this.state.form.email.name}
                typeHandle={this.typeHandler}
                elementConfig={this.state.form.email.elementConfig}
                valid={!this.state.form.email.valid}
                touched={this.state.form.email.touched}/>
            </div>
            <div className={classes.column}>
              <p>Phone: </p>
              <Input 
                elementType={this.state.form.phoneNumber.elementType} 
                name={this.state.form.phoneNumber.name}
                typeHandle={this.typeHandler}
                elementConfig={this.state.form.phoneNumber.elementConfig}
                valid={!this.state.form.phoneNumber.valid}
                touched={this.state.form.phoneNumber.touched}/>
            </div>
          </div>
          <Input 
            elementType={this.state.form.details.elementType}
            name={this.state.form.details.name}
            typeHandle={this.typeHandler}
            elementConfig={this.state.form.details.elementConfig}
            valid={!this.state.form.details.valid}
            touched={this.state.form.details.touched}/>
          <div className={classes.orderButtonWrapper}>
            <button 
              className={classes.orderButton}
              onClick={this.makeOrder}
              disabled={!(
                this.state.form.name.valid &&
                this.state.form.surname.valid &&
                this.state.form.email.valid &&
                this.state.form.phoneNumber.valid && 
                this.state.form.details.valid &&
                this.props.items
              )}>Order</button>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    items: state.cart.cart,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    makeOrder: (name, surname, email, phone, items, details, history) => dispatch(makeOrder(name, surname, email, phone, items, details, history)),
    clearCart: () => dispatch(clearCart())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Order);

import React, { Component } from "react";
import Aux from "../../hoc/Aux/Aux";
import Auth from "../Auth/Auth";
import Header from "../../Components/Header/Header";
import Logout from '../Auth/Logout/Logout';
import Categories from '../Categories/Categories';
import Items from '../Items/Items';
import FullItem from '../Items/FullItem/FullItem';
import Order from '../Order/Order';

import Cart from '../Cart/Cart'

import cartIcon from '../../assets/cart-icon.png';

import Refresh from '../../Components/Refresh/Refresh';

import { withRouter } from 'react-router-dom';

import { connect } from 'react-redux';

import { Switch } from 'react-router-dom';

import { Route } from "react-router-dom";

import classes from './Layout.css';

class Layout extends Component {
  state = {
    cartIsOpend: false
  }

  cartShowHandler = () => {
    this.setState({cartIsOpend: !this.state.cartIsOpend})
  }

  render() {
    return (
      <Aux>
        <Header />
        <Switch>
          <Route path="/auth" component={Auth}/>
          <Route path='/logout' component={Logout} />
          <Route exact path='/category' component={Categories} />
          <Route path='/category/:id' component={Items}/>
          <Route path='/item/:id' component={FullItem} />
          <Route path='/order' component={Order}/> 
          <Refresh path="/refresh"/>
          <Route path='/' component={Items} />
        </Switch>
        {this.props.isAuth 
          && (
            <React.Fragment>
              <Cart closeCartHandler={this.cartShowHandler} show={this.state.cartIsOpend}/>
              <div style={{position: 'absolute', right: 0, bottom: 0}}>
              <img onClick={this.cartShowHandler} src={cartIcon} alt='cart' className={classes.cart}/>
              </div>
            </React.Fragment>
          )}
      </Aux>
    );
  }
}

const mapStateToProps = state => {
  return {
    isAuth: state.auth.isAuth
  }
}

export default withRouter(connect(mapStateToProps, null)(Layout));

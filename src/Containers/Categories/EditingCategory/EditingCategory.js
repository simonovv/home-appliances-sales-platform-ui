import React, { Component } from "react";

import { updateCategory, deleteCategory } from "../../../store/actions/categories";

import { withRouter } from "react-router-dom";

import Dropzone from "react-dropzone";
import request from "superagent";
import isValid from "../../../utility/isValid";

import { connect } from "react-redux";

import classes from "./EditingCategory.css";

import closeIcon from "../../../assets/close-icon.png";

const CLOUDINARY_UPLOAD_PRESET = "qim6uc9u";
const CLOUDINARY_UPLOAD_URL = "https://api.cloudinary.com/v1_1/simonov/upload";

class EditingCategory extends Component {
  state = {
    uploadedFileCloudinaryUrl: "",
    newCategory: {
      id: this.props.id,
      name: {
        value: this.props.name
      },
      img: this.props.img
    }
  };

  updateCategoryHandler = () => {
    this.props.updateCategory(
      this.state.newCategory.id,
      this.state.newCategory.name.value,
      this.state.uploadedFileCloudinaryUrl || this.state.newCategory.img,
      this.props.history,
    );
  };

  typeHandler = (event, element) => {
    const newCategory = {
      ...this.state.newCategory,
      [element]: {
        ...this.state.newCategory[element],
        value: event.target.value,
        valid: isValid(
          event.target.value,
          this.state.newCategory[element].validation
        ),
        touched: true
      }
    };

    this.setState({ newCategory: newCategory });
  };

  dropzoneDropHandler = file => {
    this.setState({
      uploadedFile: file[0]
    });
    this.handleImageUpload(file[0]);
  };

  handleImageUpload = file => {
    let upload = request
      .post(CLOUDINARY_UPLOAD_URL)
      .field("upload_preset", CLOUDINARY_UPLOAD_PRESET)
      .field("file", file);

    upload.end((err, response) => {
      if (err) {
        console.error(err);
      }

      if (response.body.secure_url !== "") {
        this.setState({
          uploadedFileCloudinaryUrl: response.body.secure_url
        });
      }
    });
  };

  deleteCategoryHandler = () => {
    this.props.deleteCategory(this.state.newCategory.id, this.props.history);
  }

  render() {
    return (
      <div
        className={classes.category}
        style={{
          backgroundImage: `url(${this.state.uploadedFileCloudinaryUrl ||
            this.state.newCategory.img})`,
          backgroundSize: "cover"
        }}
      >
        <img
          alt="closeButton"
          className={classes.closeButton}
          onClick={this.props.closeHandler}
          src={closeIcon}
        />
        <input
          className={this.state.newCategory.name.touched ? this.state.newCategory.name.valid ? classes.input : [classes.input, classes.invalid].join(' ') : classes.input}
          placeholder="Name"
          type="text"
          value={this.state.newCategory.name.value}
          onChange={event => this.typeHandler(event, "name")}
        />
        <Dropzone
          onDrop={this.dropzoneDropHandler}
          className={classes.dropzone}
        >
          DropImageHere
        </Dropzone>
        <div style={{position: "absolute", bottom: 0}}>
        <button
          onClick={this.deleteCategoryHandler}
        >
          Delete
        </button>
        <button
          onClick={this.updateCategoryHandler}
        >
          Save
        </button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    refresh: state.cat.refresh
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateCategory: (id, name, img, history) => dispatch(updateCategory(id, name, img, history)),
    deleteCategory: (id, history) => dispatch(deleteCategory(id, history))
  };
};

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(EditingCategory));

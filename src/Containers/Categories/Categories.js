import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { fetchAllCategories } from '../../store/actions/categories';

import CreateCategory from '../Categories/CreateCategory/CreateCategory';
import EditingCategory from '../Categories/EditingCategory/EditingCategory';

import Category from '../../Components/Category/Category';

import newItem from '../../assets/new-item.png';

import classes from './Categories.css'

export class Categories extends Component {
    componentDidMount() {
        if(process.env.NODE_ENV !== 'test') {
            this.props.getAllCategories();
        }
    }

    state = {
        categories: null,
        creatingNewCategory: false
    }

    closeEditingHandler = (element) => {

        let newCategories = this.state.categories ? [...this.state.categories] : [...this.props.categories];

        newCategories[this.findIndex(element)].isEditing = false;

        this.setState({categories: newCategories})
    }

    editHandler = (event, element) => {
        event.stopPropagation();

        console.log('Edit');

        let newCategories = this.state.categories ? [...this.state.categories] : [...this.props.categories];

        newCategories[this.findIndex(element)].isEditing = true;

        this.setState({categories: newCategories})
    }

    searchCategoryHandler = (event) => {
        let reg = '(' + event.target.value + ')';

        let newCategories = [];

        for(let category in this.props.categories) {
            if(this.props.categories[category].name.search(reg) !== -1) {
                newCategories.push(this.props.categories[category]);
            };
        }

        this.setState({categories: newCategories})
    }

    closeHandler = () => {
        this.setState({creatingNewCategory: false});
    }

    createNewCategoryHandler = () => {
        this.setState({ creatingNewCategory: true });
    }

    selectCategoryHandler = (id) => {
        this.props.history.push(/category/ + id);
    }

    findIndex = (element) => {
        return(this.props.categories.findIndex(e => {
            return e.name === element
        }))
    }

    render() {
        const categories = this.state.categories || this.props.categories;

        return(
            <div className={classes.categories} style={{maxWidth: '1024px'}}>
                <input onKeyUp={(event) => this.searchCategoryHandler(event)} className={classes.search}/>
                <div className={classes.controls}>
                    {this.props.role === 1 &&
                        <img onClick={this.createNewCategoryHandler} src={newItem} alt='Crate new item' />}
                </div>
                <div className={classes.categoriesWrapper}>
                {this.state.creatingNewCategory && 
                    <CreateCategory 
                        closeHandler={this.closeHandler}/>}
                {categories && categories.map(category => {
                    if(category.isEditing) {
                        return <EditingCategory 
                                    key={category.id}
                                    id={category.id}
                                    name={category.name}
                                    img={category.img}
                                    closeHandler={() => this.closeEditingHandler(category.name)}/>
                    }
                    else return <Category 
                                key={category.id}
                                selectHandler={() => this.selectCategoryHandler(category.id)}
                                editHandler={(event) => this.editHandler(event, category.name)}
                                id={category.id}
                                name={category.name}
                                img={category.img}
                                role={this.props.role}/>
                })}
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        categories: state.cat.categories,
        role: state.auth.role
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getAllCategories: () => dispatch(fetchAllCategories())
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Categories));
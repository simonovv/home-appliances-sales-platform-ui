import React, { Component } from "react";

import Dropzone from "react-dropzone";
import request from "superagent";

import { connect } from "react-redux";
import { createCategory as newCategory} from "../../../store/actions/categories";

import isValid from "../../../utility/isValid";

import closeIcon from "../../../assets/close-icon.png";

import classes from "./CreateCategory.css";

const CLOUDINARY_UPLOAD_PRESET = "qim6uc9u";
const CLOUDINARY_UPLOAD_URL = "https://api.cloudinary.com/v1_1/simonov/upload";

class createCategory extends Component {
  state = {
    category: {
      name: {
        value: "",
        valid: false,
        touched: false,
        validation: {
          required: true,
          maxLength: 25,
          minLength: 3
        }
      },
      img: "none"
    }
  };

  typeHandler = (event, element) => {
    const updatedCategory = {
      ...this.state.category,
      [element]: {
        ...this.state.category[element],
        value: event.target.value,
        touched: true,
        valid: isValid(
          event.target.value,
          this.state.category[element].validation
        )
      }
    };
    this.setState({ category: updatedCategory });
  };

  dropzoneDropHandler = file => {
    this.setState({
      uploadedFile: file[0]
    });
    this.handleImageUpload(file[0]);
  };

  handleImageUpload = file => {
    let upload = request
      .post(CLOUDINARY_UPLOAD_URL)
      .field("upload_preset", CLOUDINARY_UPLOAD_PRESET)
      .field("file", file);

    upload.end((err, response) => {
      if (err) {
        console.error(err);
      }

      if (response.body.secure_url !== "") {
        this.setState({
          category: {
            ...this.state.category,
            img: response.body.secure_url
          }
        });
      }
    });
  };

  saveCategoryHandler = () => {
    this.props.createCategory(this.state.category.name.value, this.state.category.img, this.props.closeHandler);
  }

  render() {
    return (
      <div
        className={classes.category}
        style={{ backgroundImage: "url(" + this.state.category.img + ")" }}
      >
        <img
          alt='closeButton'
          className={classes.closeButton}
          onClick={this.props.closeHandler}
          src={closeIcon}
        />
        <input
          className={this.state.category.name.touched ? this.state.category.name.valid ? classes.input : [classes.input, classes.invalid].join(' ') : classes.input}
          placeholder="Name"
          type="text"
          onChange={event => this.typeHandler(event, "name")}
          autoFocus
        />
        <Dropzone
          onDrop={this.dropzoneDropHandler}
          className={classes.dropzone}
        >
          DropImageHere
        </Dropzone>
        <button style={{position: "absolute", bottom: 0}} onClick={this.saveCategoryHandler}>Save</button>
      </div>
    );
  }
}

const mapStateToPops = state => {
  return {
    error: state.cat.error
  }
}

const mapDispatchToProps = dispatch => {
  return {
    createCategory: (name, img, handler) => dispatch(newCategory(name, img, handler))
  };
};

export default connect(
  mapStateToPops,
  mapDispatchToProps
)(createCategory);

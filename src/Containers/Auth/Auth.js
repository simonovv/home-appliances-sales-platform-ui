import React, { Component } from "react";
import SingIn from "../../Components/Auth/SingIn/singIn";
import SingUp from "../../Components/Auth/SingUp/singUp";

import { connect } from "react-redux";
import Spinner from "../../Components/UI/Spinner/Spinner";

import auth from "../../store/actions/auth";
import reg from "../../store/actions/registration";
import { deleteErrorMsg } from "../../store/actions/auth";
import backIcon from '../../assets/back-icon.png';

import isValid from "../../utility/isValid";

import classes from "./Auth.css";

class Auth extends Component {
  state = {
    singIn: {
      email: {
        elementType: "input",
        elementConfig: {
          id: "email",
          type: "email",
          placeholder: "E-Mail"
        },
        touched: false,
        valid: false,
        value: "",
        validationRules: {
          required: true,
          isEmail: true
        }
      },
      password: {
        elementType: "input",
        elementConfig: {
          id: "password",
          type: "password",
          placeholder: "Password"
        },
        touched: false,
        valid: false,
        value: "",
        validationRules: {
          required: true,
          minLength: 6,
          maxLength: 25
        }
      }
    },
    singUp: {
      email: {
        elementType: "input",
        elementConfig: {
          id: "email",
          type: "email",
          placeholder: "E-Mail"
        },
        touched: false,
        value: "",
        valid: false,
        validationRules: {
          required: true
        }
      },
      password: {
        elementType: "input",
        elementConfig: {
          id: "password",
          type: "password",
          placeholder: "Password"
        },
        touched: false,
        valid: false,
        value: "",
        validationRules: {
          required: true,
          minLength: 6
        }
      },
      repeatPassword: {
        elementType: "input",
        elementConfig: {
          id: "repeatPassword",
          type: "password",
          placeholder: "Repeat password"
        },
        touched: false,
        valid: false,
        value: "",
        validationRules: {
          required: true,
          minLength: 6
        }
      }
    },
    sing: true
  };

  singInHandler = () => {
    this.props.onAuth(
      this.state.singIn.email.value,
      this.state.singIn.password.value,
      this.props.history
    );
  };

  singInTypeHandler = (event, element) => {
    let updateControls = {
      ...this.state.singIn,
      [element]: {
        ...this.state.singIn[element],
        value: event.target.value,
        valid: isValid(
          event.target.value,
          this.state.singIn[element].validationRules
        ),
        touched: true
      }
    };
    this.setState({ singIn: updateControls });
  };

  singUpTypeHandler = (event, element) => {
    const updateControls = {
      ...this.state.singUp,
      [element]: {
        ...this.state.singUp[element],
        value: event.target.value,
        valid: isValid(
          event.target.value,
          this.state.singUp[element].validationRules
        ),
        touched: true
      }
    };
    this.setState({ singUp: updateControls });
  };

  objectToArray(obj) {
    const formElementsArray = [];
    for (let key in obj) {
      formElementsArray.push({
        id: key,
        config: obj[key]
      });
    }
    return formElementsArray;
  }

  switchFormHandler = () => {
    this.props.deleteErrorMsg();
    this.setState({ sing: !this.state.sing });
  };

  signUpHandler = () => {
    this.props.reg(
      this.state.singUp.email.value,
      this.state.singUp.password.value,
      this.switchFormHandler
    );
  };

  render() {
    const loginForm = (
      <React.Fragment>
        <div className={classes.topWrapper}>
          <p><strong>SignIn</strong></p>
          {this.props.loginError 
            && <div style={{ marginBottom: "30px" }}>
              {this.props.loginError}
            </div>}
        </div>
        <SingIn
          typeHandle={this.singInTypeHandler}
          form={this.objectToArray(this.state.singIn)}/>
        <div className={classes.controls}>
          <button
            onClick={this.singInHandler}
            disabled={!(this.state.singIn.email.valid && this.state.singIn.password.valid)}>
            Login
          </button>
          <button onClick={this.switchFormHandler}>SingUp</button>
        </div>
      </React.Fragment>
    );

    const regForm = (
      <React.Fragment>
        <div className={classes.topWrapper}>
          <img src={backIcon} alt='back' onClick={this.switchFormHandler} className={classes.backButton}/>
          <p><strong>SignUp</strong></p>
          {this.props.regError 
            && <div style={{ marginBottom: "30px" }}>
              {this.props.regError}
            </div>}
        </div>
        <SingUp
          typeHandle={this.singUpTypeHandler}
          form={this.objectToArray(this.state.singUp)}/>
        <button
          onClick={this.signUpHandler}
          disabled={
            !(this.state.singUp.email.valid &&
              this.state.singUp.password.valid &&
              this.state.singUp.repeatPassword.valid
            )}>
          SignUp
        </button>
      </React.Fragment>
    );

    return (
      <div className={classes.authformwrapper}>
        <div className={classes.authform}>
          {(this.props.regLoading || this.props.loginLoading) && <Spinner />}
          {/* {(this.props.loginError || this.props.regError) && (
            <div style={{ marginBottom: "30px" }}>
              {this.props.regError && this.props.regError}
              {this.props.loginError && this.props.loginError} */}
            {/* </div>)} */}
          {this.state.sing ? 
            loginForm :
              regForm}
          </div>
        </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    loginLoading: state.auth.loading,
    loginError: state.auth.error,
    isAuth: state.auth.isAuth,
    regError: state.reg.error,
    regLoading: state.reg.loading
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onAuth: (email, password, history) => dispatch(auth(email, password, history)),
    reg: (email, password, handler) => dispatch(reg(email, password, handler)),
    deleteErrorMsg: () => dispatch(deleteErrorMsg())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Auth);

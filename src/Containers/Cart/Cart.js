import React, { Component } from 'react';
import { connect } from 'react-redux';
import { lessItem, moreItem } from '../../store/actions/cart';
import { Link } from 'react-router-dom';

import CartItem from '../../Components/Cart/CartItem/CartItem';

import classes from './Cart.css';

class Cart extends Component {
  lessHandler = (event, id) => {
    event.preventDefault();
    this.props.lessItem(id);
  }

  moreHandler = (event, id) => {
    event.preventDefault();
    this.props.moreItem(id);
  }

  render() {
    const cart = (
      <div className={classes.cartWrapper} style={this.props.show ? { height: '385px' } : { height: '0px' }}>
        <div className={classes.cart}>
          <p className={classes.title}>Cart</p>
          {this.props.cart.map(item => {
            return <CartItem id={item.id} amount={item.amount} key={item.id} name={item.name} price={item.price} img={item.img} lessHandler={this.lessHandler} moreHandler={this.moreHandler}/>
          })}
          {this.props.cart.length !== 0 && <Link to='/order'><button onClick={this.props.closeCartHandler} className={classes.order}>Continue</button></Link>}
        </div>
        <div className={classes.arrow}>
        </div>
      </div>
    );

    return (
      <React.Fragment>
        {cart}
      </React.Fragment>
    )
  }
}

const mapStateToProps = state => {
  return {
    cart: state.cart.cart
  }
}

const mapDispatchToProps = dispatch => {
  return {
    moreItem: (name) => dispatch(moreItem(name)),
    lessItem: (name) => dispatch(lessItem(name))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Cart);